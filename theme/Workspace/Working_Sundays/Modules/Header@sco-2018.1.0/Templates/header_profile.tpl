{{#if showExtendedMenu}}
	<a class="header-profile-welcome-link" href="#" data-toggle="dropdown">
		<i class="header-profile-welcome-user-icon"></i>
		<div style="color: #c13dff;">
			{{translate 'WELCOME <strong class="header-profile-welcome-link-name">$(0)</strong>' displayName}}
		</div>
		{{!-- 
		<i class="header-profile-welcome-carret-icon"></i>
		--}}
	</a>

	{{#if showMyAccountMenu}}
		<ul class="header-profile-menu-myaccount-container" style="
			width: 100vw;
			margin-left: -50vw;
			left: 50%; 
		">
			<li data-view="Header.Menu.MyAccount"></li>
		</ul>
	{{/if}}

{{else}}

	{{#if showLoginMenu}}
		{{#if showLogin}}
			<div class="header-profile-menu-login-container">
				<ul class="header-profile-menu-login">
					<li>
						<a class="header-profile-login-link" data-touchpoint="login" data-hashtag="login-register" href="#">
							<i class="header-profile-login-icon"></i>
							<div style="color: #c13dff; display: inline;">
								{{translate 'LOGIN'}}
							</div>
						</a>
					</li>
					{{#if showRegister}}
						<li> | </li>
						<li>
							<a class="header-profile-register-link" data-touchpoint="register" data-hashtag="login-register" href="#">
							<div style="color: #c13dff; display: inline;">
								{{translate 'REGISTER'}}
							</div>
							</a>
						</li>
					{{/if}}
				</ul>
			</div>
		{{/if}}
	{{else}}
		<a class="header-profile-loading-link">
			<i class="header-profile-loading-icon"></i>
			<span class="header-profile-loading-indicator"></span>
		</a>
	{{/if}}

{{/if}}



{{!----
Use the following context variables when customizing this template:

	showExtendedMenu (Boolean)
	showLoginMenu (Boolean)
	showLoadingMenu (Boolean)
	showMyAccountMenu (Boolean)
	displayName (String)
	showLogin (Boolean)
	showRegister (Boolean)

----}}
